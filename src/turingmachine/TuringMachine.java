/*
 * Guatemala, 10 de mayo de 2019
 */
package turingmachine;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @author Ernesto Cap
 */
public class TuringMachine extends javax.swing.JFrame {
    //variables 
    int contador=0;
    String[][] matrizAGenerada = new String[3][3];
    String[][] matrizBGenerada = new String[3][3];
    String[][] matrizCGenerada = new String[3][3];
    String[][] matrizDGenerada = new String[3][3];
    int[][] cintaGenerada = new int[1][9];
    private static String binario;
    private static int longitud;
    private static int decimal;
    
    
    /*
     * Creates new form TuringMachine
     */
    
    public TuringMachine() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        matrizA = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        matrizB = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        matrizC = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        matrizD = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        cinta = new javax.swing.JTable();
        generarMatriz = new javax.swing.JButton();
        generarCinta = new javax.swing.JButton();
        estadoTextField = new javax.swing.JTextField();
        reglasLabel = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        decimalText = new javax.swing.JTextField();
        iniciarMaquinaButton = new javax.swing.JButton();
        leeTextField = new javax.swing.JTextField();
        nuevoEstadoTextField = new javax.swing.JTextField();
        escribeTextField = new javax.swing.JTextField();
        estadoLabel = new javax.swing.JLabel();
        leeLabel = new javax.swing.JLabel();
        nuevoEstadoLabel = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        verResultado = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        matrizA.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "", "", ""
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(matrizA);
        if (matrizA.getColumnModel().getColumnCount() > 0) {
            matrizA.getColumnModel().getColumn(0).setResizable(false);
            matrizA.getColumnModel().getColumn(1).setResizable(false);
            matrizA.getColumnModel().getColumn(2).setResizable(false);
        }

        matrizB.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "", "", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(matrizB);
        if (matrizB.getColumnModel().getColumnCount() > 0) {
            matrizB.getColumnModel().getColumn(0).setResizable(false);
            matrizB.getColumnModel().getColumn(1).setResizable(false);
            matrizB.getColumnModel().getColumn(2).setResizable(false);
        }

        matrizC.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "", "", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(matrizC);
        if (matrizC.getColumnModel().getColumnCount() > 0) {
            matrizC.getColumnModel().getColumn(0).setResizable(false);
            matrizC.getColumnModel().getColumn(1).setResizable(false);
            matrizC.getColumnModel().getColumn(2).setResizable(false);
        }

        matrizD.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "", "", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(matrizD);
        if (matrizD.getColumnModel().getColumnCount() > 0) {
            matrizD.getColumnModel().getColumn(0).setResizable(false);
            matrizD.getColumnModel().getColumn(1).setResizable(false);
            matrizD.getColumnModel().getColumn(2).setResizable(false);
        }

        jLabel1.setText("A");

        jLabel2.setText("B");

        jLabel3.setText("C");

        jLabel4.setText("D");

        cinta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "", "", "", "", "", "", "", "", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane5.setViewportView(cinta);
        if (cinta.getColumnModel().getColumnCount() > 0) {
            cinta.getColumnModel().getColumn(0).setResizable(false);
            cinta.getColumnModel().getColumn(1).setResizable(false);
            cinta.getColumnModel().getColumn(2).setResizable(false);
            cinta.getColumnModel().getColumn(3).setResizable(false);
            cinta.getColumnModel().getColumn(4).setResizable(false);
            cinta.getColumnModel().getColumn(5).setResizable(false);
            cinta.getColumnModel().getColumn(6).setResizable(false);
            cinta.getColumnModel().getColumn(7).setResizable(false);
            cinta.getColumnModel().getColumn(8).setResizable(false);
        }

        generarMatriz.setText("GENERAR MATRICES");
        generarMatriz.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generarMatrizActionPerformed(evt);
            }
        });

        generarCinta.setText("GENERAR CINTA");
        generarCinta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generarCintaActionPerformed(evt);
            }
        });

        estadoTextField.setEditable(false);
        estadoTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                estadoTextFieldActionPerformed(evt);
            }
        });

        reglasLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        reglasLabel.setText("REGLAS");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"0", "0", "0", "0"},
                {"0", "1", "1", "0"},
                {"1", "0", "2", "1"},
                {"1", "1", "1", "1"},
                {"2", "0", "2", "0"},
                {"2", "1", "2", "1"}
            },
            new String [] {
                "Estado", "Lee", "Nuevo Estado", "Escribe"
            }
        ));
        jScrollPane6.setViewportView(jTable1);

        jLabel5.setText("RESULTADO EN DECIMAL");

        iniciarMaquinaButton.setText("INICIAR");
        iniciarMaquinaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                iniciarMaquinaButtonActionPerformed(evt);
            }
        });

        leeTextField.setEditable(false);

        nuevoEstadoTextField.setEditable(false);

        escribeTextField.setEditable(false);

        estadoLabel.setText("Estado");

        leeLabel.setText("Lee");

        nuevoEstadoLabel.setText("N Estado");

        jLabel6.setText("Escribe");

        verResultado.setText("Ver Resultado");
        verResultado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verResultadoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 1199, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addGap(55, 55, 55)
                                        .addComponent(jLabel3)
                                        .addGap(129, 129, 129)
                                        .addComponent(jLabel4))
                                    .addComponent(generarMatriz, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(reglasLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel5)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(decimalText, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(verResultado))
                                .addGap(200, 200, 200))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 367, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(iniciarMaquinaButton, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(153, 153, 153))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(92, 92, 92)
                        .addComponent(jLabel1)
                        .addGap(124, 124, 124)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(estadoTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(estadoLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(leeTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
                            .addComponent(leeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nuevoEstadoLabel)
                            .addComponent(nuevoEstadoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(escribeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(213, 213, 213)
                                .addComponent(generarCinta, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel6))))
                .addGap(66, 66, 66))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(77, 77, 77)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(61, 61, 61)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel2)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(generarCinta)))
                        .addGap(23, 23, 23)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(26, 26, 26)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(generarMatriz)
                                .addGap(18, 18, 18)
                                .addComponent(reglasLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addComponent(decimalText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(verResultado)
                                .addGap(632, 632, 632))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(estadoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(leeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nuevoEstadoTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(escribeTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(leeLabel)
                            .addComponent(estadoLabel)
                            .addComponent(nuevoEstadoLabel)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(iniciarMaquinaButton, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(235, 235, 235))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void generarMatrizActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generarMatrizActionPerformed
        //el evento muestra las matrices en las tablas del formulario
        this.mostrarMatriz(this.generarMatriz());
    }//GEN-LAST:event_generarMatrizActionPerformed

    private void generarCintaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generarCintaActionPerformed
        // TODO add your handling code here:
        this.mostrarCinta(this.generarCinta());
    }//GEN-LAST:event_generarCintaActionPerformed

    private void iniciarMaquinaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_iniciarMaquinaButtonActionPerformed
        // TODO add your handling code here:
        this.compararValores();
    }//GEN-LAST:event_iniciarMaquinaButtonActionPerformed

    private void estadoTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_estadoTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_estadoTextFieldActionPerformed

    private void verResultadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_verResultadoActionPerformed
        // TODO add your handling code here:
        this.convertirADecimal();
    }//GEN-LAST:event_verResultadoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TuringMachine.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TuringMachine.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TuringMachine.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TuringMachine.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TuringMachine().setVisible(true);
            }
        });
    }
    
    public void mostrarMatriz(String[][] matriz){       
        
        switch (contador) {                
            case 0:
                for(int i=0;i<3;i++){
                    for(int j=0;j<3;j++){
                        matrizA.setValueAt(matriz[i][j], i, j);
                    }
                }
                contador++;
                matrizAGenerada = matriz;
                System.out.println("Contador0   " + contador );
                break;
            case 1:
                for(int i=0;i<3;i++){
                    for(int j=0;j<3;j++){
                        matrizB.setValueAt(matriz[i][j], i, j);
                    }
                }
                contador++;
                matrizBGenerada = matriz;
                System.out.println("Contador1   " + contador );
                break;
            case 2:
                for(int i=0;i<3;i++){
                    for(int j=0;j<3;j++){
                        matrizC.setValueAt(matriz[i][j], i, j);
                    }
                }
                contador++;
                matrizCGenerada = matriz;
                System.out.println("Contador2   " + contador );
                break;
            case 3:
                for(int i=0;i<3;i++){
                    for(int j=0;j<3;j++){
                        matrizD.setValueAt(matriz[i][j], i, j);
                    }
                }
                contador++;
                matrizDGenerada = matriz;
                System.out.println("Contador3   " + contador );
                break;
            default:
                if(contador>=4){
                    contador=0;
                    System.out.println("Contador   " + contador );
                }
                break;   
        }
        }
    
    public String[][] generarMatriz(){
        String[][] matriz=new String[3][3]; 
        String[] lista={"A","B","C","D","0","1","L","F","R","N","0","0","1","1","1","1","1","1","1","0","F","0","0","1","1","1","F","1","1","1","0","1","1","0","1","1"};
        
        int index;
        String temp;
        Random random = new Random();
        
        for (int i = lista.length - 1; i > 0; i--)
        {
            index = random.nextInt(i + 1);
            temp = lista[index];
            lista[index] = lista[i];
            lista[i] = temp;
           // System.out.println(lista[i]);
        }
        
        /*for (int i = 0; i < 9; i++) {
            //System.out.println("indice: "+i+ " = " +lista[i]);
        }
        */
        
         matriz[0][0]=lista[0];
         matriz[0][1]=lista[1];
         matriz[0][2]=lista[2];
         matriz[1][0]=lista[3];
         matriz[1][1]=lista[4];
         matriz[1][2]=lista[5];
         matriz[1][0]=lista[6];
         matriz[2][1]=lista[7];
         matriz[2][2]=lista[8];
         matriz[2][0]=lista[9];
       
            return matriz;
        }
    
    
    public void mostrarCinta(int[][] datos){
        for(int i=0;i<1;i++){
            for(int j=0;j<9;j++){
            cinta.setValueAt(datos[i][j], i, j);
            }
        }
        cintaGenerada = datos;
    }
    
    public int[][] generarCinta(){
        int[][] datosCinta=new int[1][9]; 
        int[] datos = {1,0,1,1,0,1,0,1,1,0,1,1,0,0,0,1,0,0};
        
        int index;
        int temp;
        Random random = new Random();
        
        for (int i = datos.length - 1; i > 0; i--)
        {
            index = random.nextInt(i + 1);
            temp = datos[index];
            datos[index] = datos[i];
            datos[i] = temp;
            //System.out.println(datos[i]);
        }
        
        datosCinta[0][0] = datos[0];
        datosCinta[0][1] = datos[1];
        datosCinta[0][2] = datos[2];
        datosCinta[0][3] = datos[3];
        datosCinta[0][4] = datos[4];
        datosCinta[0][5] = datos[5];
        datosCinta[0][6] = datos[6];
        datosCinta[0][7] = datos[7];
        datosCinta[0][8] = datos[8];
        
        
        return datosCinta;
    }
    
    public void compararValores(){
        int contadorC = 0;
        for(int i= 0;i < matrizAGenerada.length; i++){
            for(int j = 0; j < matrizAGenerada[0].length; j++ ){
                
                System.out.println("Fila: " + i + " Columna: " + j + " Contenido: " + matrizAGenerada[i][j]);
             if(matrizAGenerada[i][j] == "0"){
                 if(cintaGenerada[0][contadorC] == 0){
                     estadoTextField.setText("0");
                     leeTextField.setText("0");
                     nuevoEstadoTextField.setText("0");
                     System.out.println("La regla no afecta la matriz");
                     escribeTextField.setText("0");
                     System.out.println("La regla no afecta la cinta");
                     contadorC++;
                 }else if(cintaGenerada[0][contadorC] == 1){
                     estadoTextField.setText("0");
                     leeTextField.setText("1");
                     nuevoEstadoTextField.setText("1");
                     System.out.println("La regla si afecta la matriz");
                     matrizAGenerada[i][j] = "1";
                     matrizA.setValueAt("1", i, j);
                     escribeTextField.setText("0");
                     cintaGenerada[0][contadorC] = 0;
                     this.mostrarCinta(cintaGenerada);
                     contadorC++;
                 }
             }  
                    
            }
            
        }
        contadorC = contadorC - 1;
        System.out.println(contadorC);
        
    }
    
    public void convertirADecimal(){
          int numero;
     
        String cadenaB= Arrays.deepToString(cintaGenerada);
        cadenaB = cadenaB.replace("[", "");
        cadenaB = cadenaB.replace("]", "");
        cadenaB = cadenaB.replace(",", "");
        System.out.println(cadenaB);
        
        numero = Integer.parseInt(cadenaB);
        System.out.println(numero);
        int decimal = 0;
        int binary = numero;
        int power = 0;
 
        while (binary != 0) {
            int lastDigit = binary % 10;
            decimal += lastDigit * Math.pow(2, power);
            power++;
            binary = binary / 10;
        }
        
        decimalText.setText(Integer.toString(decimal));
        System.out.println(decimal);
      
    }
     
    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable cinta;
    private javax.swing.JTextField decimalText;
    private javax.swing.JTextField escribeTextField;
    private javax.swing.JLabel estadoLabel;
    private javax.swing.JTextField estadoTextField;
    private javax.swing.JButton generarCinta;
    private javax.swing.JButton generarMatriz;
    private javax.swing.JButton iniciarMaquinaButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel leeLabel;
    private javax.swing.JTextField leeTextField;
    private javax.swing.JTable matrizA;
    private javax.swing.JTable matrizB;
    private javax.swing.JTable matrizC;
    private javax.swing.JTable matrizD;
    private javax.swing.JLabel nuevoEstadoLabel;
    private javax.swing.JTextField nuevoEstadoTextField;
    private javax.swing.JLabel reglasLabel;
    private javax.swing.JButton verResultado;
    // End of variables declaration//GEN-END:variables
}
